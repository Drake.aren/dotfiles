#!/bin/sh

#speed up keyboard
xset r rate 190 20

# setup keyboard layout
setxkbmap \
  -model pc104 \
  -layout us,ru \
  -variant qwert_digits \
  -option grp:caps_toggle

sh ~/.fehbg &
nm-applet &
guake &
#wal -i $(cat ~/.wallpaper) &
#dropbox-cli start &

compton -b

