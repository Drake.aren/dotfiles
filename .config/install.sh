#!/bin/sh

sudo pacman -Sy

# sman
bash -c "$(curl https://raw.githubusercontent.com/tokozedg/sman/master/install.sh)"

if [[ ! -d ~/snippets ]]; then
  git clone git@gitlab.com:Drake.aren/sman-snippets.git ~/snippets
fi

# Yay dependencies
sudo pacman -S --needed \
  go \
  git \
  base-devel

if [[ ! $(command -v yay) ]]
then
  YAY_DIR=/tmp/yay
  git clone https://aur.archlinux.org/yay.git $YAY_DIR
  cd $YAY_DIR
  makepkg -si
  cd -
fi

yay -Sy \
  light \
  vivaldi \
  polybar

sudo pacman -S --needed \
  feh \
  rofi \
  fzf \
  fzy \
  the_silver_searcher \
  i3 \
  termite \
  xclip \
  fd \
  cloc \
  xclip \
  otf-fira-code \
  adobe-source-code-pro-fonts \
  neofetch \
  slim \
  acpi \
  networkmanager \
  neovim \
  network-manager-applet \
  scrot \
  imagemagick \
  ranger \
  compton \
  guake \
  lxappearance-gtk3 \

