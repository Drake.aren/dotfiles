""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.config/nvim/plugged')
  " Elixir
  Plug 'mhinz/vim-mix-format'
  Plug 'elixir-editors/vim-elixir'
  " JavaScript bundle for vim
  " this bundle provides syntax highlighting and improved indentation.
  Plug 'pangloss/vim-javascript'
  Plug 'junegunn/limelight.vim'
  Plug 'dylanaraps/wal'
  " Fuzz finder
  Plug '/usr/bin/fzf'
  Plug 'junegunn/fzf.vim'
  " Debug
  Plug 'vim-vdebug/vdebug'
  " Bookmark
  Plug 'MattesGroeger/vim-bookmarks'
  " Minimap
  Plug 'severin-lemaignan/vim-minimap'
  " Motion
  Plug 'easymotion/vim-easymotion'
  " Color hightlight
  Plug 'ap/vim-css-color'
  " Syntacsis highlight
  Plug 'vim-syntastic/syntastic'
  "
  Plug 'tpope/vim-eunuch'
  " File manager
  Plug 'scrooloose/nerdtree'
  " Clojure
  Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
  "Plug 'clojure-vim/acid.nvim', { 'for': 'clojure', 'do': ':UpdateRemotePlugins' }
  " Go
  Plug 'fatih/vim-go', { 'for': 'go', 'do': ':GoUpdateBinaries' }
  " PHP
  Plug 'beanworks/vim-phpfmt', { 'for': 'php' }
  Plug 'StanAngeloff/php.vim', { 'for': 'php' }
  " Color theme
  Plug 'morhetz/gruvbox'
  Plug 'joshdick/onedark.vim'
  Plug 'dracula/vim', { 'as': 'dracula' }
  " Git
  Plug 'airblade/vim-gitgutter'
  " Airline
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  " Nuake
  Plug 'lenovsky/nuake'
  " Vue
  Plug 'posva/vim-vue'
  " Pug
  Plug 'digitaltoad/vim-pug'
  " Line number toggler
  Plug 'jeffkreeftmeijer/vim-numbertoggle'
  " Code search
  Plug 'brooth/far.vim'
  " Ident vertical line
  Plug 'Yggdroot/indentLine'
  " Start screen
  Plug 'mhinz/vim-startify'
  " Complementions
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
  " Snippet's
  Plug 'honza/vim-snippets'
  Plug 'SirVer/ultisnips'
call plug#end()
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Quick comment
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let s:comment_map = {
    \   "c": '\/\/',
    \   "cpp": '\/\/',
    \   "go": '\/\/',
    \   "java": '\/\/',
    \   "javascript": '\/\/',
    \   "lua": '--',
    \   "scala": '\/\/',
    \   "php": '\/\/',
    \   "python": '#',
    \   "ruby": '#',
    \   "rust": '\/\/',
    \   "sh": '#',
    \   "desktop": '#',
    \   "fstab": '#',
    \   "conf": '#',
    \   "profile": '#',
    \   "bashrc": '#',
    \   "bash_profile": '#',
    \   "mail": '>',
    \   "eml": '>',
    \   "bat": 'REM',
    \   "ahk": ';',
    \   "vim": '"',
    \   "tex": '%',
    \ }

function! ToggleComment()
    if has_key(s:comment_map, &filetype)
        let comment_leader = s:comment_map[&filetype]
        if getline('.') =~ "^\\s*" . comment_leader . " "
            " Uncomment the line
            execute "silent s/^\\(\\s*\\)" . comment_leader . " /\\1/"
        else
            if getline('.') =~ "^\\s*" . comment_leader
                " Uncomment the line
                execute "silent s/^\\(\\s*\\)" . comment_leader . "/\\1/"
            else
                " Comment the line
                execute "silent s/^\\(\\s*\\)/\\1" . comment_leader . " /"
            end
        end
    else
        echo "No comment leader found for filetype"
    end
endfunction

" inoremap <M-/> :call ToggleComment()<cr>
nnoremap <M-/> :call ToggleComment()<cr>
vnoremap <M-/> :call ToggleComment()<cr>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Command
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <silent> <space> :Commands <enter>
nnoremap <silent> <C-space> :Maps <enter>
command Search :Ag
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Elixir
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:mix_format_on_save = 1
let g:mix_format_options = '--check-equivalent'
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command ReloadConfig :so $HOME/.config/nvim/init.vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Limelight
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
"let g:limelight_conceal_ctermfg = 240
"autocmd VimEnter * Limelight
"let g:limelight_priority = -1
"let g:limelight_paragraph_span = 1
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Movment
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <silent> <A-Up> :wincmd k<Enter>
inoremap <silent> <A-Down> :wincmd j<Enter>
inoremap <silent> <A-Right> :wincmd l<Enter>
inoremap <silent> <A-Left> :wincmd h<Enter>

nnoremap <A-j> :m .+1<CR>== nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Easy motion
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" <Leader>f{char} to move to {char}
map  <Leader>f <Plug>(easymotion-bd-f)
nmap <Leader>f <Plug>(easymotion-overwin-f)

" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)

" Move to line
map <Leader>L <Plug>(easymotion-bd-jk)
nmap <Leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <Leader>w <Plug>(easymotion-bd-w)
nmap <Leader>w <Plug>(easymotion-overwin-w)

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Bookmark
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:bookmark_auto_save_file = $HOME .'/.config/nvim/.bookmarks'
" To copy to clipboard
set clipboard=unnamedplus

" Search
let g:CoolTotalMatches = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" PHP
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:phpfmt_autosave = 0
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Git
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Show GTM in status bar
let g:gtm_plugin_status_enabled = 1

function! AirlineInit()
  if exists('*GTMStatusline')
    call airline#parts#define_function('gtmstatus', 'GTMStatusline')
    let g:airline_section_b = airline#section#create([g:airline_section_b, ' ', '[', 'gtmstatus', ']'])
  endif
endfunction
autocmd User AirlineAfterInit call AirlineInit()
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Snippet's
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntastic
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_php_checkers = ['php']
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Complementions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:deoplete#enable_at_startup = 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Color cheme
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Define color sheme
color dracula
" Set color sheme theme
set bg=dark
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Function folding
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"
" map + to toggle fold under cursor
"noremap + za
"
" Enable by default
"set foldmethod=indent
"set foldlevel=3
"set foldclose=all
"command AutoFoldingToggle :foldenable
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" IdentLine
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:indentLine_color_term = 239
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" highlight search matches
set hlsearch
" search while characters are entered
set incsearch
" No highlight serach result on ESC
noremap <silent> <ESC> :noh <Enter>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Other
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Trim trailing space on save
autocmd BufWritePre * :%s/\s\+$//e
" Mouse support
set mouse=a
" Automatically update a file if it is changed externally
set autoread
" Height of the command bar
set cmdheight=2
noremap <silent> <C-S> :update<Enter>
vnoremap <silent> <C-S> <C-C> :update<Enter>
inoremap <silent> <C-S> <C-O> :update<Enter>
inoremap <silent> <C-Z> <C-O> :undo<Enter>
" Line number
set number relativenumber
" Show line under cursor
set cursorline
" show last command in the bottom right
set showcmd
" always show current position
set ruler
" Wrap line at 80 char positioned
set colorcolumn=80
" show matching braces
set showmatch
" Enable syntax highlighting
syntax on
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Word wraping
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command WordWrapEnable :set wrap
command WordWrapDisable :set nowrap
" wrap only at valid characters
set linebreak
" prevent vim from inserting linebreaks
set textwidth=0
" in newly entered text
set wrapmargin=0
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File ident
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" UTF-8 encoding and en_US as default encoding/language
set encoding=utf-8
" Define standard filetype
set fileencoding=utf-8
set fileencodings=ucs-bom,utf-8,gbk,cp936,latin-1
set fileformat=unix
set fileformats=unix,dos,mac
" recognize .md files as markdown files
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
" enable spell-checking for markdown files
autocmd BufRead,BufNewFile *.md setlocal spell
" enable filetype specific indentation
" enable filetype specific indentation
filetype on
filetype plugin on
filetype plugin indent on
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Characte ident
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ident hidden symvols
"set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·
set listchars=tab:>·,trail:~,extends:>,precedes:<,space:·
set list
" modern backspace behavior
set backspace=indent,eol,start
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indent
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tab size
set tabstop=2
set softtabstop=0
" Dont use actual `tab` character
set expandtab
set shiftwidth=2
set smarttab
set autoindent
" does the right thing (mostly) in programs
set smartindent
" stricter rules for C programs
set cindent
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Nuake
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <M-Enter> :Nuake<Enter>
inoremap <M-Enter> <C-\><C-n>:Nuake<Enter>
tnoremap <M-Enter> <C-\><C-n>:Nuake<Enter>
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" File autosave
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set autowrite
augroup AutoWrite
  autocmd! BufLeave * :write
  autocmd! BufLeave * :update
augroup END
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTree
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nnoremap <silent> <A-TAB> :NERDTreeToggle <Enter>

autocmd StdinReadPre * let s:std_in=1
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | Startify | endif

" NERDTress File highlighting
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
 exec 'autocmd filetype nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
 exec 'autocmd filetype nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
endfunction

call NERDTreeHighlightFile('pug', 'green', 'none', 'green', '#151515')
call NERDTreeHighlightFile('ini', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('md', 'blue', 'none', '#3366FF', '#151515')
call NERDTreeHighlightFile('yml', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('config', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('conf', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('json', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('html', 'yellow', 'none', 'yellow', '#151515')
call NERDTreeHighlightFile('styl', 'cyan', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('css', 'cyan', 'none', 'cyan', '#151515')
call NERDTreeHighlightFile('coffee', 'Red', 'none', 'red', '#151515')
call NERDTreeHighlightFile('js', 'Red', 'none', '#ffa500', '#151515')
call NERDTreeHighlightFile('php', 'Magenta', 'none', '#ff00ff', '#151515')
