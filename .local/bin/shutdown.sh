#!/bin/sh

function git_sync () {
  cd $1
  git pull
  git add .
  git commit -m 'sync'
  git push
  cd -
}

git_sync $HOME/snippets

# Yadm
cd $HOME
yadm add -u
yadm commit -m 'sync'
yadm pull
yadm push

