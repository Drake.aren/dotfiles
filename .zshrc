source ~/.config/envrc

# Antigen
if [[ ! -f $XDG_CONFIG_HOME/antigen.zsh ]]
then
  curl -L git.io/antigen > ~/.config/antigen.zsh
fi
source $XDG_CONFIG_HOME/antigen.zsh

# Wal - Color scheme manager base on wallpapers
#if [[ ! $(command -v wal) ]]
#then
#  DIST=~/.local/bin/wal &&\
#  curl https://raw.githubusercontent.com/dylanaraps/wal/master/wal \
#  > $DIST ;\
#  chmod +x $DIST
#fi

#(cat ~/.cache/wal/sequences &)

if [[ $(command -v direnv) ]]
then
  eval "$(direnv hook zsh)"
fi

# Load the oh-my-zsh's library.
antigen use oh-my-zsh

# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle git
antigen bundle sudo
antigen bundle battery
antigen bundle command-not-found
antigen bundle adb
antigen bundle archlinux
antigen bundle history-substring-search

# Syntax highlighting bundle.
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle supercrabtree/k
antigen bundle gko/ssh-connect
antigen bundle b4b4r07/enhancd

# Load the theme.
antigen theme https://github.com/denysdovhan/spaceship-prompt spaceship

# Tell Antigen that you're done.
antigen apply

bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line

HISTFILE="$HOME/.zsh_history"
COMPLETION_WAITING_DOTS="true"
ENABLE_CORRECTION="true"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[ -f ~/.sman/sman.rc ] && source ~/.sman/sman.rc

export PATH=$PATH:~/.sman/bin

source $XDG_CONFIG_HOME/ssh/sshrc
source $XDG_CONFIG_HOME/aliasrc

