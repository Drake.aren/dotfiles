# Dotfiles

## Install 

```shell
git clone \
    --recurse-submodules \
    git@gitlab.com:Drake.aren/dotfiles.git; \
sh $HOME/install.sh
```